import React, { useCallback, useContext } from 'react';
import styles from './animalSelector.module.css';
import {AnimalContext} from "../../context/animalContext";

const AnimalSelector = () => {
    const animals = ['cat', 'dog'];
    const { animalDispatch } = useContext(AnimalContext);

    const handleAnimalChange = useCallback(e => {
        animalDispatch({
            type: 'SET_ANIMAL',
            payload: e.target.value
        });
    }, [animalDispatch]);

    return (
        <form
            className={styles.animalSelectorSection}
        >
            <h2>Ik ben een</h2>
            <div className={styles.animalSelector}>
                {
                    animals.map((item, i) => (
                        <div className={styles.inputWrap} key={i}>
                            <label htmlFor={item}>
                                <img src={`/${item}.jpg`} alt={item}/>
                                {item === 'dog' ? 'Hondenmens' : 'Kattenmens'}
                            </label>
                            <input
                                type="radio"
                                id={item}
                                name="animalSelector"
                                value={item}
                                onChange={e => handleAnimalChange(e)}
                            />
                        </div>
                    ))
                }
            </div>
        </form>
    );
}

export default AnimalSelector;