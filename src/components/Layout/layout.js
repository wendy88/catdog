import React from 'react';
import styles from './layout.module.css';
import Header from '../Header/header';
import AnimalSelector from '../AnimalSelector/animalSelector';
import BreedSelector from "../BreedSelector/breedSelector";
import { AnimalProvider } from "../../context/animalContext";

const Layout = () => (
    <div className={styles.layoutWrapper}>
        <AnimalProvider>
            <Header />
            <main className={styles.main}>
                <AnimalSelector />
                <BreedSelector />
            </main>
        </AnimalProvider>
    </div>
)

export default Layout;