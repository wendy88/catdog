import React, { useContext, useCallback, useState } from 'react';
import axios from 'axios';
import styles from './breedSelector.module.css';
import { AnimalContext } from "../../context/animalContext";

const BreedSelector = () => {
    const { animalState } = useContext(AnimalContext);
    const [breed, setBreed] = useState('');
    const [error, setError] = useState(false);
    const [animalData, setAnimalData] = useState(null);
    // Fix with switch case and allowedAnimal data when more animals are added
    const animal = animalState && animalState.animal && animalState.animal !== ""
        ? animalState.animal === "dog"
            ? "honden"
            : "katten"
        : "";

    const handleBreedChange = useCallback(e => {
        setBreed(e.target.value);
    }, [setBreed]);

    const submitAnimalForm = useCallback(async e => {
        e.preventDefault();
        // Because of time i'm doing this a bit raw/hard coded
        // Normally I would make a custom hook for the data fetching that returns data, error, loading
        const url = animalState && animalState.animal && animalState.animal !== ""
            ? animalState.animal === "dog"
                ? `https://dog.ceo/api/breed/${breed}/images/random`
                : `https://api.thecatapi.com/v1/breeds/search?q=${breed}`
            : "";

        if(animalState && animalState.animal && animalState.animal === 'cat')
            axios.defaults.headers.common['x-api-key'] = '129bf7b6-c59d-4d63-b617-3c084c2a083d';

        try {
            const { data } = await axios(url);
            animalState.animal && animalState.animal === 'dog'
            ? setAnimalData({'image': data.message})
            : setAnimalData({'description': data[0].description})
        } catch(err) {
            setError(true);
        }

        setBreed('');
    }, [breed, animalState, setBreed, setAnimalData, setError])

    return (
        animalState && animalState.animal && animalState.animal !== "" ?
            <React.Fragment>
                <form
                    className={styles.breedSelectorSection}
                    onSubmit={e => submitAnimalForm(e)}
                >
                    <h2>{`Mijn favoriete ${animal}ras is een`}</h2>
                    <div className={styles.inputWrap}>
                        <input
                            type="text"
                            value={breed}
                            onChange={e => handleBreedChange(e)}
                        />
                        <button className={styles.button} type="submit">Toon mijn favorietje</button>
                    </div>
                </form>
                {
                    !error && animalData && animalData.image &&
                        <div className={styles.output}><img src={animalData.image} alt="dog"/></div>

                }
                {
                    !error && animalData && animalData.description &&
                        <div className={styles.output}>
                            <h3>Fun Fact</h3>
                            <p>{animalData.description}</p>
                        </div>
                }
                {
                    error &&
                        <p className={styles.output}>Helaas dit ras is nog niet bekend, probeer iets anders.</p>
                }
            </React.Fragment>
            : null
    );
}

export default BreedSelector;