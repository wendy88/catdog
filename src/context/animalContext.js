import React, { useReducer } from 'react';
import { animalReducer } from '../reducer/animalReducer';

export const AnimalContext = React.createContext();

export const AnimalProvider = ({ children }) => {
    const [animalState, animalDispatch] = useReducer(animalReducer, {
        animal: ''
    });

    return (
        <AnimalContext.Provider
            value={{
                animalState,
                animalDispatch,
            }}
        >
            {children}
        </AnimalContext.Provider>
    );
};