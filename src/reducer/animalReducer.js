export const animalReducer = (state, action) => {
    switch (action.type) {
        case 'SET_ANIMAL': {
            return { animal: action.payload };
        }
        default: {
            return state;
        }
    }
};